export default class AuthModel {

    constructor(api) {
        this.api = api;
    }

    provideCreateSessionRequest = (credentialData) => {
        return this.api.createSessionRequest(credentialData);
    };

    saveToken = (data) => {
        sessionStorage.setItem('vToken', data);

    };

    getToken = () => {
        localStorage.getItem('token');
    }
}
