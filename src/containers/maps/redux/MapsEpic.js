import * as actions from './actions/MapsAction';
import {toast} from 'react-toastify';
import * as types from './actions/MapsTypes';

export const createFinedInputRequestEpic = (action$, store, {mapsModel}) => {
    return action$
        .ofType(types.FINED_INPUT_REQUEST)
        .debounceTime(200)
        .switchMap(action =>
            mapsModel
                .createFinedInputRequest(action.payload)
                .map(response => {
                    toast.info(`${action.payload.address} has been update`);
                    return actions.createFinedInputRequestSuccess(response)
                })
        );
};
export const fetchLocationsEpic = (action$, store, {mapsModel}) => {
    return action$
        .ofType(types.RAW_LOCATIONS_REQUEST)
        .debounceTime(200)
        .switchMap(action =>
            mapsModel
                .fetchRawLocations()
                .map(response => {
                    return actions.fetchRawLocationsSuccess(response.data ? response.data.locations : [])
                })
        );
};
