import {combineReducers} from 'redux';
import LayoutReducer from '../../components/Layout/redux/LayoutReducer';
import AuthReducer from '../../containers/auth/redux/AuthReducer';
import PlayReducer from '../../containers/app1/redux/PlayReducer';
import MapsReducer from '../../containers/maps/redux/MapsReducer';

const RootReducer = combineReducers({
    layout: LayoutReducer,
    auth: AuthReducer,
    play: PlayReducer,
    maps: MapsReducer
});

export default RootReducer
