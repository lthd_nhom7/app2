import {combineEpics} from 'redux-observable';
import {
    createSessionRequestEpic,
    createSessionSuccessEpic
} from '../../containers/auth/redux/AuthEpic';
import {
    createRawInputRequestEpic
} from '../../containers/app1/redux/PlayEpic';
import {createFinedInputRequestEpic, fetchLocationsEpic} from "../../containers/maps/redux/MapsEpic";

const RootEpic = combineEpics(
    createSessionRequestEpic,
    createSessionSuccessEpic,
    createRawInputRequestEpic,
    createFinedInputRequestEpic,
    fetchLocationsEpic
);

export default RootEpic
