import React, {Component} from 'react';
import 'rxjs';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import AppRoute from '../routes/AppRoute';
import Init from '../components/Init';
import socketIOClient from "socket.io-client";

class App extends Component {
    // constructor() {
    //     super();
    //     this.state = {
    //         response: false,
    //         endpoint: "http://0.0.0.0:7000"
    //     };
    // }
    //
    // componentDidMount() {
    //     const {endpoint} = this.state;
    //     const socket = socketIOClient(endpoint);
    //     socket.on("FromAPI", data => console.log('data', data));
    // }

    render() {
        return (
            <div>
                <Init/>
                <AppRoute/>
            </div>
        );
    }
}

export default App;
