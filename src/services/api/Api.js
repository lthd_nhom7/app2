import ApiBase from './ApiBase';

export default class Api {

    constructor() {
        this.apiBase = new ApiBase();
        this.token = ''
    }

    createSessionRequest = (credentialData) => {
        const requestData = {
            url: '/users/authenticate',
            data: credentialData
        };
        return this.apiBase.callPost(requestData);
    };

    createRawInputRequest = (rawData) => {
        const requestData = {
            url: '/locations',
            data: rawData
        };
        return this.apiBase.callPost(requestData);
    };
    createFinedInputRequest = (rawData) => {
        const requestData = {
            url: `/locations/${rawData.id}`,
            data: rawData
        };
        return this.apiBase.callPut(requestData);
    };
    fetchRawLocations = () => {
        const requestData = {
            url: '/locations',
            data: null
        };
        return this.apiBase.callGet(requestData);
    };

    setAuthToken = (token) => {
        this.token = token
    }
}
